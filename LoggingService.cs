﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeeklyDashboard
{
    public class LoggingService
    {
        //private const string _logFileLocation = @"C:\Programming\Jobs\DeltaPrevention\VisualStudioProject\WeeklyDashboard\WeeklyDashboard\bin\Release\net6.0\publish\servicelog.txt";
        private const string _logFileLocation = @"C:\ScriptsRunning\weeklyDashboardExportService\servicelog.txt";

        public void Log(string logMessage)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(_logFileLocation));
            File.AppendAllText(_logFileLocation, DateTime.UtcNow.ToString() + " : " + logMessage + Environment.NewLine);
        }
    }
}
