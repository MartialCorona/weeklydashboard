﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeeklyDashboard.Models
{
    public class MessageGmail
    {
        public byte[] bytes { get; set; }
        public bool isOnly { get; set; }
    }
}
