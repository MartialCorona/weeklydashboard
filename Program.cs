﻿using System.ServiceProcess;

namespace WeeklyDashboard
{
    public class Program
    {
        private static WeeklyDashboardService _googleService;

        public static void Main(string[] args)
        {
            GetService();
            //ServiceBase.Run(new LoggingService());

        }

        static void GetService()
        {
            _googleService = new WeeklyDashboardService();
            _googleService.dataManipulation();
        }
    }
}