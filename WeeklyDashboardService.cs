﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WeeklyDashboard.Models;

namespace WeeklyDashboard
{
    public class WeeklyDashboardService
    {
        private const string CommandLineWeeklyDashboardDelta = "from:(supportBI@deltaprev.onmicrosoft.com) subject:(01 Weekly Dashboard Delta) after:{0}";
        private const string CommandLineWeeklyDashboardMSK = "from:(supportBI@deltaprev.onmicrosoft.com) subject:(02 Weekly Dashboard MSK) after:{0}";
        private const string aliasGmail = "support@deltaprevention.com";
        private const string folderImagesPathDelta = @"\\192.168.9.2\delta\IT\PROJECTS\967 - Highlights\WEEKLY DASHBOARD\DELTA\IMAGES\W{0}";
        private const string folderImagesPathMSK = @"\\192.168.9.2\delta\IT\PROJECTS\967 - Highlights\WEEKLY DASHBOARD\MSK\IMAGES\W{0}";
        private GmailService ServiceGoogle;

        #region Properties
        private readonly LoggingService _logger;
        private CultureInfo cul = CultureInfo.CurrentCulture;
        private DateTime CurrentDate = DateTime.Now;
        private string AfterDate { get => CurrentDate.ToString("yyyy/MM/dd"); }
        private GmailService Service { get; set; }
        
        private string threadId = "";
        #endregion


        public WeeklyDashboardService()
        {
            _logger = new LoggingService();
        }

        /// <summary>
        /// Purpose : Main method
        /// </summary>
        /// <returns></returns>
        public void dataManipulation()
        {
            try
            {
                int counter = 0;
                var messageListDelta = GetDataGmail(aliasGmail, String.Format(CommandLineWeeklyDashboardDelta, AfterDate));
                var messageListMSK = GetDataGmail(aliasGmail, String.Format(CommandLineWeeklyDashboardMSK, AfterDate));


                if (messageListDelta != null)
                {
                    foreach (var message in messageListDelta)
                    {
                        var messageBytesList = GetBytesAttachmentMessage(message);
                        var fullMessage = Service.Users.Messages.Get("me", message.Id).Execute();
                        SendMessage(message, "support@deltaprevention.com", messageBytesList[0].bytes, counter);
                        break;
                    }
                    counter++;
                }
                if (messageListMSK != null)
                {
                    foreach (var message in messageListMSK)
                    {
                        var messageBytesList = GetBytesAttachmentMessage(message);
                        var fullMessage = Service.Users.Messages.Get("me", message.Id).Execute();
                        SendMessage(message, "support@deltaprevention.com", messageBytesList[0].bytes , counter);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Log(e.ToString());
                throw;
            }

        }

        /// <summary>
        /// Purpose : Connexion with gmail API service and getting data
        /// </summary>
        /// <param name="accountGmail">Credential to connect to the windows service</param>
        /// <param name="commandLine">Command line to send accross the API</param>
        /// <param name="fetchingMessage">Boolean</param>
        /// <returns></returns>
        public List<Message> GetDataGmail(string aliasGmail, string commandLine)
        {
            try
            {
                string serviceAccountEmail = "tic-service@quickstart-1575510092344.iam.gserviceaccount.com";

                var certificate = new X509Certificate2(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\quickstart-1575510092344-66b2c30070c1.p12", "notasecret", X509KeyStorageFlags.Exportable);

                ServiceAccountCredential credential = new ServiceAccountCredential(
                   new ServiceAccountCredential.Initializer(serviceAccountEmail)
                   {
                       Scopes = new[] { GmailService.Scope.GmailReadonly, GmailService.Scope.GmailCompose, GmailService.Scope.GmailModify },
                       User = aliasGmail
                   }.FromCertificate(certificate));

                // Create the service.
                var service = new GmailService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "TIC-Email-Extract",
                });

                Service = service;
                ServiceGoogle = Service;

                return ListMessages(Service, "me", commandLine);

            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Transform attachment to bytes and put it in MessageGmail model
        /// </summary>
        public List<MessageGmail> GetBytesAttachmentMessage(Message message)
        {
            try
            {
                string incoming = "";
                List<MessageGmail> messageBytesList = new List<MessageGmail>();

                if (message != null)
                {

                    var fetchedMessageList = GetMessage(Service, "me", message.Id);
                    threadId = message.ThreadId;
                    foreach (var fetchedMessage in fetchedMessageList)
                    {
                        incoming = fetchedMessage.Data
                      .Replace('_', '/').Replace('-', '+');
                        switch (fetchedMessage.Data.Length % 4)
                        {
                            case 2: incoming += "=="; break;
                            case 3: incoming += "="; break;
                        }
                        messageBytesList.Add(new MessageGmail
                        {
                            bytes = Convert.FromBase64String(incoming),
                        });
                    }
                }
                return messageBytesList;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Send a new message with a format attachment in the same thread ID from the initial message
        /// </summary>
        /// <param name="service"></param>
        /// <param name="to"></param>
        /// <param name="from"></param>
        /// <param name="subject"></param>
        /// <param name="bodyText"></param>
        /// <param name="stream"></param>
        /// <param name="threadId"></param>
        /// <returns></returns>
        public void SendMessage(Message message , String from, byte[] bytes , int counter)
        {
            try
            {
                if (counter == 0)
                {
                    List<string> fileNames = new List<string>();
                    MimeMessage email = new MimeMessage();

                    email.From.Add(new MailboxAddress("Support Delta", from));
                    email.To.AddRange(new List<MailboxAddress> { new MailboxAddress("Maxime Deslauriers", "mdeslauriers@deltaprevention.com"),
                                                                 new MailboxAddress("Daniel Wu", "dwu@deltaprevention.com"),
                                                                 new MailboxAddress("Dominique Lafrance", "dlafrance@deltaprevention.com"),
                                                                 new MailboxAddress("Élodie Boudreault", "eboudreault@mskcanada.com"),
                                                                 new MailboxAddress("Jean-François Gravel", "jfgravel@deltaprevention.com"),
                                                                 new MailboxAddress("Kevin Key", "kkey@deltaprevention.com"),
                                                                 new MailboxAddress("Marie-Claude Robidoux", "mrobidoux@mskcanada.com"),
                                                                 new MailboxAddress("Myriam Allard", "mallard@mskcanada.com"),
                                                                 new MailboxAddress("Pierre-Olivier Benoit", "pobenoit@deltaprevention.com"),
                                                                 new MailboxAddress("Sandryne Martel", "smartel@deltaprevention.com"),
                                                                 new MailboxAddress("Edwars Chang", "echang@deltaprevention.com"),
                                                                 new MailboxAddress("Gabriel Serra", "gserra@deltaprevention.com"),
                                                                 new MailboxAddress("Mikael Robert", "mrobert@deltaprevention.com"),
                                                                 new MailboxAddress("Michel Paré", "mpare@deltaprevention.com"),
                                                                 new MailboxAddress("Stéphane Key", "stkey@deltaprevention.com"),
                    });


                    var builder = new BodyBuilder();
                    int currentWeekNum = cul.Calendar.GetWeekOfYear(CurrentDate, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
                    string[] filePaths = Directory.GetFiles(string.Format(folderImagesPathDelta, currentWeekNum));
                    string subject = "Weekly Dashboard Delta -Week " + currentWeekNum.ToString();
                    email.Subject = subject;


                    var imagePowerBI = builder.LinkedResources.Add("WeeklyDashboard.png", bytes, ContentType.Parse("image/png"));
                    imagePowerBI.ContentId = Guid.NewGuid().ToString();
                    string htmlBody = string.Format("<img width=\"{0}\" src=\"cid:{1}\"> <br> <h1>Images : </h1>", 1400, imagePowerBI.ContentId);


                    foreach (var imgPath in filePaths)
                    {
                        string? fileName = Path.GetFileName(imgPath);
                        if (fileName != "Thumbs.db")
                        {
                            string? trimFileName = fileName.Remove(fileName.Length - 4);
                            var otherImage = builder.LinkedResources.Add(imgPath);
                            otherImage.ContentId = Guid.NewGuid().ToString();
                            htmlBody += string.Format("<h3>{0}</h3> <img width=\"{1}\" width=\"{2}\" src=\"cid:{3}\"> <br>", trimFileName, 400, 300, otherImage.ContentId);
                        }                     
                    }

                    builder.HtmlBody = htmlBody;
                    email.Body = builder.ToMessageBody();

                    ServiceGoogle.Users.Messages.Send(new Message
                    {
                        Raw = Base64UrlEncode(email),
                        ThreadId = message.ThreadId

                    }, "me").Execute();
                }

                if (counter == 1)
                {
                    List<string> fileNames = new List<string>();
                    MimeMessage email = new MimeMessage();

                    email.From.Add(new MailboxAddress("Support MSK", from));
                    email.To.AddRange(new List<MailboxAddress> { new MailboxAddress("Maxime Deslauriers", "mdeslauriers@deltaprevention.com"),
                                                                 new MailboxAddress("Louis-Philippe Bertrand", "lpbertrand@mskcanada.com"),
                                                                 new MailboxAddress("Élodie Boudreault", "eboudreault@mskcanada.com"),
                                                                 new MailboxAddress("Jean-François Gravel", "jfgravel@deltaprevention.com"),
                                                                 new MailboxAddress("Kevin Key", "kkey@deltaprevention.com"),
                                                                 new MailboxAddress("Marie-Claude Robidoux", "mrobidoux@mskcanada.com"),
                                                                 new MailboxAddress("Myriam Allard", "mallard@mskcanada.com"),
                                                                 new MailboxAddress("Pierre-Olivier Benoit", "pobenoit@deltaprevention.com"),
                                                                 new MailboxAddress("Info MskCanada", "info@mskcanada.com"),
                                                                 new MailboxAddress("Nancy Gamache", "ngamache@mskcanada.com"),
                                                                 new MailboxAddress("Serge Key", "skey@mskcanada.com"),
                                                                 new MailboxAddress("Stéphane Key", "stkey@mskcanada.com"),
                                                                 new MailboxAddress("William Key", "wkey@mskcanada.com"),
                                                                 new MailboxAddress("Ann-Frédérik Fortin", "affortin@mskcanada.com"),
                                                                 new MailboxAddress("Sandryne Martel", "smartel@deltaprevention.com"),
                                                                 new MailboxAddress("Edwars Chang", "echang@deltaprevention.com"),
                                                                 new MailboxAddress("Mikael Robert", "mrobert@deltaprevention.com"),
                                                                 new MailboxAddress("Gabriel Serra", "gserra@mskcanada.com"),
                                                                 });
                    
                    var builder = new BodyBuilder();
                    int currentWeekNum = cul.Calendar.GetWeekOfYear(CurrentDate, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
                    string[] filePaths = Directory.GetFiles(string.Format(folderImagesPathMSK, currentWeekNum));
                    string subject = "Weekly Dashboard MSK -Week " + currentWeekNum.ToString();
                    email.Subject = subject;


                    var imagePowerBI = builder.LinkedResources.Add("WeeklyDashboard.png", bytes, ContentType.Parse("image/png"));
                    imagePowerBI.ContentId = Guid.NewGuid().ToString();
                    string htmlBody = string.Format("<img width=\"{0}\" src=\"cid:{1}\"> <h1>Images : </h1>", 1400, imagePowerBI.ContentId);


                    foreach (var imgPath in filePaths)
                    {
                        string? fileName = Path.GetFileName(imgPath);
                        if (fileName != "Thumbs.db")
                        {
                            string? trimFileName = fileName.Remove(fileName.Length - 4);
                            var otherImage = builder.LinkedResources.Add(imgPath);
                            otherImage.ContentId = Guid.NewGuid().ToString();
                            htmlBody += string.Format("<h3>{0}</h3> <img width=\"{1}\" width=\"{2}\" src=\"cid:{3}\">  <br>", trimFileName, 400, 300, otherImage.ContentId);
                        }                 
                    }

                    builder.HtmlBody = htmlBody;
                    email.Body = builder.ToMessageBody();

                    ServiceGoogle.Users.Messages.Send(new Message
                    {
                        Raw = Base64UrlEncode(email),
                        ThreadId = message.ThreadId

                    }, "me").Execute();
                }

            }
            catch (Exception)
            {

                throw;
            }
        }




        /// <summary>
        /// List all Messages of the user's mailbox matching the query.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="query">String used to filter Messages returned.</param>
        public List<Message> ListMessages(GmailService service, String userId, String query)
        {
            try
            {
                List<Message> result = new List<Message>();
                UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List(userId);
                request.Q = query;

                do
                {
                    try
                    {
                        ListMessagesResponse response = request.Execute();

                        if (response.Messages != null)
                        {
                            result.AddRange(response.Messages);
                            request.PageToken = response.NextPageToken;
                        }
                        else
                        {
                            result = null;
                            break;
                        }

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                } while (!String.IsNullOrEmpty(request.PageToken));

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// Retrieve a Message by ID and return messagePartBody.
        /// </summary>
        /// <param name="service">Gmail API service instance.</param>
        /// <param name="userId">User's email address. The special value "me"
        /// can be used to indicate the authenticated user.</param>
        /// <param name="messageId">ID of Message to retrieve.</param>
        public List<MessagePartBody> GetMessage(GmailService service, String userId, String messageId)
        {
            try
            {
                String idAttachment = "";
                Message actualMessageRessource = new Message();
                List<MessagePartBody> msgPartBody = new List<MessagePartBody>();

                actualMessageRessource = service.Users.Messages.Get(userId, messageId).Execute();

                if (actualMessageRessource.Payload.PartId != null)
                {
                    foreach (var item in actualMessageRessource.Payload.Parts)
                    {
                        if (item.Filename != "")
                        {
                            idAttachment = item.Body.AttachmentId;
                            msgPartBody.Add(service.Users.Messages.Attachments.Get(userId, messageId, idAttachment).Execute());
                        }
                    }
                }

                return msgPartBody;
            }
            catch (Exception)
            {
                throw;
            }

        }


        /// <summary>
        /// Encode a mime message into Base 64 URL
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private string Base64UrlEncode(MimeMessage message)
        {
            try
            {
                using (var stream = new MemoryStream())
                {
                    message.WriteTo(stream);

                    return Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length)
                        .Replace('+', '-')
                        .Replace('/', '_')
                        .Replace("=", "");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
